import axios from 'axios';
import { AuthService } from '../src/service/AuthService';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

test('should return an access token', async () => {
    const response = {
        "access_token": "",
        "expires_in": 600,
        "refresh_expires_in": 1800,
        "refresh_token": "",
        "token_type": "bearer",
        "id_token": "",
        "not-before-policy": 0,
        "session_state": "",
        "scope": "openid email profile"
    };
    mockedAxios.post.mockResolvedValue({ data: response });
    const authService = new AuthService(
        {
            authUrl: 'http://localhost:8080',
            clientId: 'keycloak-demo-client-id',
            clientSecret: '2e968a47-9bab-46ae-903f-1e296ebf3d69'
        }
    );
    const data = await authService.getToken('admin', 'admin');
    expect(mockedAxios.post).toHaveBeenCalled();
    expect(data).toBe(response);
});

